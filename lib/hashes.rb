# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  hashy = {}
  str.split.each do |word|
    hashy[word] = word.length
  end
  hashy
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|k, v| v}.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  older.update(newer)
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  hash = Hash.new(0)
  word.each_char {|c| hash[c] += 1 }
  hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  new_arr = []
  arr.each do |el|
    new_arr << el unless new_arr.include?(el)
  end
  new_arr
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hash = {even: 0, odd: 0}
  numbers.each do |num|
    hash[:even] += 1 if num.even?
    hash[:odd] += 1 if num.odd?
  end
  hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  hash = {'a' => 0, 'e' => 0, 'i' => 0, 'o' => 0, 'u' => 0}
  string.each_char do |letter|
    hash[letter] += 1 if hash.key?(letter)
  end

  arr = hash.sort_by {|k, v| v}
  if arr[-1][1] == arr[-2][1]
    if arr[-1][0] > arr[-2][0]
      return arr[-2][0]
    else
      return arr[-1][0]
    end
  end
  arr[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  second_half = []
  students.each { |k, v| second_half << k if v > 6 }

  combos = []
  i = 0
  while i < second_half.length
    x = i + 1
    while x < second_half.length
      combos << [second_half[i], second_half[x]]
      x += 1
    end
    i += 1
  end
  combos.uniq
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  hashy = Hash.new(0)
  specimens.each do |el|
    hashy[el] += 1
  end
  smallest = 10
  largest = 0
  hashy.each_pair do |k, v|
    if v > largest
      largest = v
    end
    if v < smallest
      smallest = v
    end
  end

  specimen_count = hashy.length
  ((specimen_count**2) * smallest) / largest
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal = normal_sign.downcase.split(//).sort
  vandal = vandalized_sign.downcase.split(//).sort
  normal.delete(".,/ ?'! ")
  vandal.delete(".,/ ?'! ")

  i = 0
  while i < normal.length
    x = 0
    while x < vandal.length
      if normal[i] == vandal[x]
        normal.delete_at(i)
        vandal.delete_at(x)
        i -= 1
        x -= 1
      end
      x += 1
    end
    i += 1
  end
  return true if vandal.length == 0
  false
end

def character_count(str)
end
